//
//  _0230207_PiyushGoel_NYCSchoolsTests.swift
//  20230207-PiyushGoel-NYCSchoolsTests
//
//  Created by PG on 2/7/23.
//

import XCTest
@testable import _0230207_PiyushGoel_NYCSchools

final class _0230207_PiyushGoel_NYCSchoolsTests: XCTestCase {

    //MARK:- UnitTest Case for SchoolParser API for Valid Response
    func test_SchoolParserAPI_WithValidURL_Returns_SchoolAPIResponse()
    {
        //ARRANGE
        let apiResource = SchoolParser()
        let expectation = self.expectation(description: "ValidURL_Returns_SchoolAPIResponse")
        //ACT
        apiResource.apiToGetSchoolData { (schoolDataResponse) in
            
            let firstDataValue = schoolDataResponse.first!
            //ASSERT
            XCTAssertNotNil(schoolDataResponse)
            XCTAssertEqual("Clinton School Writers & Artists, M.S. 260",firstDataValue.schoolName)
            XCTAssertEqual("02M260", firstDataValue.dbNumber)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    //MARK:- UnitTest Case for SATScoreParser API for Valid Response
    func test_SATScoreParserAPI_WithValidURL_Returns_SATScoreResponse()
    {
        //ARRANGE
        let apiResource = SATScoreParser()
        let expectation = self.expectation(description: "ValidURL_Returns_SATScoreResponse")
        //ACT
        apiResource.apiToGetSATScoreData(dbnValue: "01M292") { (SATScoreResponse) in
            
            let firstDataValue = SATScoreResponse.first!
            //ASSERT
            XCTAssertNotNil(SATScoreResponse)
            XCTAssertEqual("404",firstDataValue.mathScore)
            XCTAssertEqual("355", firstDataValue.readingScore)
            XCTAssertEqual("363", firstDataValue.writingScore)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }

}
