//
//  SchoolParser.swift
//  20230207-PiyushGoel-NYCSchools
//
//  Created by PG on 2/7/23.
//

import UIKit

class SchoolParser: NSObject {

    //MARK:- Define apiToGetSchoolData function
    func apiToGetSchoolData(completion: @escaping ([SchoolDetail]) -> ())
    {
        if let url = URL(string: Constants.schoolURL) {
            URLSession.shared.dataTask(with: url) {data, response, error in
                do {
                    if error == nil
                    {
                        let schoolData = try
                            JSONDecoder().decode([SchoolDetail].self, from: data!)
                        completion(schoolData)
                    }
                }
                catch
                {
                    print(Constants.errorString + "\(error.localizedDescription)")
                }
                
            }.resume()
        }
    }
}
