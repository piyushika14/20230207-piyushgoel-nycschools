//
//  SATScoreParser.swift
//  20230207-PiyushGoel-NYCSchools
//
//  Created by PG on 2/7/23.
//

import UIKit

class SATScoreParser: NSObject {

    //MARK:- Define apiToGetSATScoreData function
    func apiToGetSATScoreData(dbnValue: String,completion: @escaping ([SATScoreDetail]) -> ())
    {
        if let url = URL(string: Constants.SATScoreURL + "\(dbnValue)") {
            URLSession.shared.dataTask(with: url) {data, response, error in
                do {
                    if error == nil
                    {
                        let SATScoreData = try
                            JSONDecoder().decode([SATScoreDetail].self, from: data!)
                        completion(SATScoreData)
                    }
                }
                catch
                {
                    print(Constants.errorString + "\(error.localizedDescription)")
                }
            }.resume()
        }
    }
}
