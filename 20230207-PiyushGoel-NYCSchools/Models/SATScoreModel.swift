//
//  SATScoreModel.swift
//  20230207-PiyushGoel-NYCSchools
//
//  Created by PG on 2/7/23.
//

import Foundation

//MARK:- SatScoreDetails

struct SATScoreDetail: Decodable,Equatable {
    
    let mathScore: String
    let readingScore: String
    let writingScore: String
    
    enum CodingKeys: String, CodingKey {
        case mathScore = "sat_math_avg_score"
        case readingScore = "sat_critical_reading_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
