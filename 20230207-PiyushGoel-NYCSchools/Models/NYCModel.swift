//
//  NYCModel.swift
//  20230207-PiyushGoel-NYCSchools
//
//  Created by PG on 2/7/23.
//

import Foundation

//MARK:- SchoolDetail

struct SchoolDetail: Decodable {
    let dbNumber: String
    let schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbNumber = "dbn"
        case schoolName = "school_name"
    }
}
