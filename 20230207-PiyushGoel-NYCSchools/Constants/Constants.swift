//
//  Constants.swift
//  20230207-PiyushGoel-NYCSchools
//
//  Created by PG on 2/7/23.
//

import Foundation

//MARK:- Declare Constants

struct Constants {
    static let schoolURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let SATScoreURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    static let errorString = "Error in getting JSON"
    static let schoolList = "List Of NYC High Schools"
    static let SchoolTableViewCell = "SchoolTableViewCell"
    static let DetailViewController = "DetailViewController"
}
