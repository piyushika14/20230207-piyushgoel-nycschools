//
//  SchoolViewController.swift
//  20230207-PiyushGoel-NYCSchools
//
//  Created by PG on 2/7/23.
//

import UIKit

//MARK:- Declare SchoolViewController
class SchoolViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var schoolParser : SchoolParser!
    
    var jsonData = [SchoolDetail]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.schoolParser = SchoolParser()
        self.navigationItem.title = Constants.schoolList
        callFuncToGetSchoolName()
    }
    
    func callFuncToGetSchoolName() {
        self.schoolParser.apiToGetSchoolData { (schoolData) in
            self.jsonData = schoolData
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}

//MARK:- Declare SchoolViewController extension
extension SchoolViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.SchoolTableViewCell, for: indexPath) as! SchoolTableViewCell
        
        //configure your cell
        cell.schoolNameLabel.text = jsonData[indexPath.row].schoolName
        cell.accessoryType = .disclosureIndicator
        cell.updateConstraintsIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dbno = jsonData[indexPath.row].dbNumber
        var viewController: DetailViewController
        viewController = self.storyboard?.instantiateViewController(withIdentifier: Constants.DetailViewController) as! DetailViewController
        viewController.dbnValue = dbno
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
