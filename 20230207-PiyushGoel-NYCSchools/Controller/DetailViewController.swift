//
//  DetailViewController.swift
//  20230207-PiyushGoel-NYCSchools
//
//  Created by PG on 2/7/23.
//

import UIKit

//MARK:- Declare DetailViewController class
class DetailViewController: UIViewController {

    private var satScoreParser : SATScoreParser!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    var dbnValue : String = ""
    var mathScoreValue = ""
    var writingScoreValue = ""
    var readingScoreValue = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.satScoreParser = SATScoreParser()
        callFuncToGetSATScores()
    }
    
    //MARK:- Define function to get SAT Scores
    func callFuncToGetSATScores() {
        self.satScoreParser.apiToGetSATScoreData(dbnValue: dbnValue) { (satScoreData) in
            if (satScoreData != [])
            {
                for value in satScoreData
                {
                    self.mathScoreValue = value.mathScore
                    self.writingScoreValue = value.writingScore
                    self.readingScoreValue = value.readingScore
                    DispatchQueue.main.async
                    {
                        self.mathScoreLabel.text = self.mathScoreValue
                        self.readingScoreLabel.text = self.readingScoreValue
                        self.writingScoreLabel.text = self.writingScoreValue
                    }
                }
            }
        }
    }
}
